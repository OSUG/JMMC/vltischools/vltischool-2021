#!/bin/bash

set -e
# get passwords from files
X2PASS=$(cat vltis.password) 
X2PASS_VLTI=$(cat vlti.password) 

for i in $( seq 1 22 )
do
  NB=$(printf "%02d" $i)
  X2HOST=vs2021-$NB.jmmc.fr X2USER=vlti 
  echo  "ssh $X2USER@$X2HOST S=\$(ps |grep xfce4-session | cut -d ' ' -f 1); export SESSION_MANAGER=local/\$HOSTNAME:@/tmp/.ICE-unix/\$S,unix/\$HOSTNAME:/tmp/.ICE-unix/\$S ; xfdesktop --reload"
done



# todo replace with faster commands
#echo "# List group sessions"
#for i in $( seq 1 22 )
#do
#  NB=$(printf "%02d" $i)
#  X2HOST=vs2021-$NB.jmmc.fr X2USER=vlti-0$NB 
#  echo "D=\$(pyhoca-cli --debug --force-password --add-to-known-hosts --server $X2HOST --user $X2USER --password $X2PASS -L 2>/dev/null| grep display | sed 's/display: //g'  ) "
#  echo $X2HOST : $D
#done

echo 
echo

echo "# Join group sessions"
for i in $( seq 1 22 )
do
  NB=$(printf "%02d" $i)
  X2HOST=vs2021-$NB.jmmc.fr X2USER=vlti-0$NB 
  echo pyhoca-cli --debug --force-password --add-to-known-hosts --share-mode 1 --sound none --kbd-layout fr --kbd-type pc105/fr --server $X2HOST --user $X2USER --password $X2PASS --share-desktop $X2USER@:50
done

echo 
echo

echo "# Create or resume group sessions"
for i in $( seq 1 22 )
do
  NB=$(printf "%02d" $i)
  X2HOST=vs2021-$NB.jmmc.fr X2USER=vlti-0$NB
  echo pyhoca-cli --debug --force-password --add-to-known-hosts  --server $X2HOST --user $X2USER --password $X2PASS --kbd-layout us --kbd-type pc105/us -N -c XFCE --try-resume -g 1680x1050 \&
done

echo 
echo

echo "# Create or resume vlti sessions"
for i in $( seq 1 22 )
do
  NB=$(printf "%02d" $i)
  X2HOST=vs2021-$NB.jmmc.fr X2USER=vlti 
  echo pyhoca-cli --debug --force-password --add-to-known-hosts  --server $X2HOST --user $X2USER --password $X2PASS_VLTI --kbd-layout us --kbd-type pc105/us -N -c XFCE --try-resume -g 1680x1050 
done


