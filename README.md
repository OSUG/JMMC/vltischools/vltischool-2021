# vltischool-2021

Manage material so 2021 vltischool's students get the better support for practices sessions

This project handles :
- virtualbox image creation using *vagrant* 
- provisioning on the [GRICAD/NOVA platform](https://gricad-doc.univ-grenoble-alpes.fr/nova/) using *terraform*/*packer* 
- machine installation using *ansible* 

Most material reuse past experience code but we could think about more modularity:
- submodules for ansible roles
- ...


# VAGRANT recipe

## Prerequisites:

On your linux host:
```bash
sudo apt install vagrant ansible
```


## Virtual Machine setup

To start buiding, run: 
```bash
vagrant up
```

If you modify the ansible playbook 'school-playbook.yml' or roles recepices and want to apply changes, run: 
```bash
vagrant provision
```

Virtual machine details are stored into the 'Vagrantfile'.
To export the built machine, run:
```bash
vagrant package
```

You can also try 'ssh' or 'destroy' at the end

Command helper t omanage x2go sessions
```bash

# create or resumt a master session forcing a us keyboard so next client will get automatic translation support on fly (I hope)
pyhoca-cli --debug --force-password --add-to-known-hosts  --server $X2HOST --user $X2USER --password $X2PASS --kbd-layout us --kbd-type pc105/us -N -c XFCE --try-resume -g 1680x1050

# list running sessions
pyhoca-cli --debug --force-password --add-to-known-hosts  --server $X2HOST --user $X2USER --password $X2PASS -L

# get a shared session access with your right (simple) keyboard  model and layout codes - :50 may differ depending on past operations
pyhoca-cli --debug --force-password --add-to-known-hosts --share-mode 1 --sound none --kbd-layout fr --kbd-type pc105/fr --server $X2HOST --user $X2USER --password $X2PASS --share-desktop $X2USER@:50
```
