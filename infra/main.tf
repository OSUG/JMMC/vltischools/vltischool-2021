module "vlti_learninglab" {
  source  = "remche/learninglab/openstack"
  version = "0.4.2"
  user_count = 30
  user_prefix = "vlti"
  same_password = true
  image_id = "40070199-2607-4bb7-94b5-2f4f32eb65f5"
  flavor = "m1.xxlarge"
  hostname_prefix = "vs2021"
  key_pair = "mellag-rc"
  instance_count   = 22
  floating_ip_network = "dmz"
  dns_domain = "jmmc.fr."
  dns_nameservers = ["152.77.1.22", "195.83.24.30"]
  shared_volume = true
  volume_size = 500
  volume_mount_point = "/data"
}

output "users" {
  sensitive = true
  value = module.vlti_learninglab.users
}

output "instances" {
  sensitive = true
  value = module.vlti_learninglab.instances
}
